const Course = require('../models/Course');

//Create a new course
/*
Steps:
	1. Create a new Course object using the mongoose model and the information from teh request body.
	2. Save the new Course to the database 

*/

module.exports.addCourse = (reqBody) => {
	
	//create a variable "newCourse" and instantiate the name, description, price
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	//Save the create object to our database
	return newCourse.save().then((course, error) => {
		//Course creation is succssful or not
		if (error) {
			return false;
		} else {
			return newCourse.save().then((user, err) => {
			if (user) {
				return user;
			} else {
				return 'Failed to Register account';
			}
		});
		}
	});
}

//Retrieve all courses

//1. Retrieve all courses from the database

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	}).catch(error => error)
}

//Retrieve all active courses

//1. Retrieve all active courses from the database
module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result;
	}).catch(error => error)
}

//Retrieving a specific course
//1. Retrieve the course that matches teh course ID provided from the URL

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	}).catch(error => error)
}

//UPDATE a course
/*
Steps: 
	1. Create a variable "updatedCourse" which will contain the info retrieved from the req.body
	2.Find and Update the course using the courseId retrieved from the req.params and the variable "updatedCourse" contaning info from req.body

*/
module.exports.updateCourse = (courseId, data) => {
	//specify the fields/properties of the dcument to be updated
	let updatedCourse = {
		name: data.name,
		description: data.description,
		price: data.price
	}

	//findByIdAndUpdate(document Id, updatesTobeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		} else {
			return updatedCourse;
		}
	}).catch(error => error)
}

//Archiving a course
//1. update the status of "isActive" into "false" which will no longer be displayed in the client whenever all active courses are retrived

module.exports.archiveCourse = (courseId) => {
	let updateActiveField = {
		isActive: false
	};

	return Course.findByIdAndUpdate( courseId, updateActiveField).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}

//Activating a course
module.exports.activateCourse = (courseId) => {
	let updateActiveField = {
		isActive: true
	};

	return Course.findByIdAndUpdate( courseId, updateActiveField).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}